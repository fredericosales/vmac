# Virtual Machines starting on headless virtualbox server

### vbox@.service 
---

```ini
Description=Virtual Box Guest %I
After=network.target vboxdrv.service
Before=runlevel2.target shutdown.target

[Service]
Type=forking
Restart=no
TimeoutSec=5min
IgnoreSIGPIPE=no
KillMode=process
GuessMainPID=no
RemainAfterExit=yes

ExecStart=/usr/bin/vboxmanage startvm %i --type headless
ExecStop=/usr/bin/vboxmanage controlvm %i poweroff

[Install]
WantedBy=multi-user.target
```

---
### Install
---

```bash
user@machine[~/]$: git clone https://gitlab.com/fredericosales/vmac.git
```
```bash
user@machine[~/]$: cd vmac
```

```bash
user@machine[~/]$: cp vbox@.service to ~/.config/systemd/user
```

```bash
user@machine[~/]$: systemctl --user reload-daemon
```

```bash
user@machine[~/]$: vboxmanage list vms
```

```bash
user@machine[~/]$: vboxmanage modifyvm vm-name --vrde on
```

```bash
user@machine[~/]$: systemctl --user start vbox@vm-name.service
```
---
### Remote Display (VRDP support)
---

By default VRDP server uses TCP port 3389.  You will need to change the default port if you run more than one VRDP server, since the port can only be used by one server at a time. You might also need to change it on Windows hosts since the default port might already be used by the RDP server that is built into Windows itself. Ports 5000 through 5050 are typically not used and might be a good choice.

The port can be changed either in the Display settings of the graphical user interface or with the --vrdeport option of the VBoxManage modifyvm command. You can specify a comma-separated list of ports or ranges of ports. Use a dash between two port numbers to specify a range. The VRDP server will bind to one of the available ports from the specified list. For example, VBoxManage modifyvm VM-name --vrdeport 5000,5010-5012 configures the server to bind to one of the ports 5000, 5010, 5011, or 5012.

The actual port used by a running VM can be either queried with the VBoxManage showvminfo command or seen in the GUI on the Runtime tab of the Session Information dialog, which is accessible from the Machine menu of the VM window. 

Ref.: [Virtualbox manual chapter 7](https://www.virtualbox.org/manual/ch07.html)

---
### Scripts
---

### listvms

```bash
#!/bin/bash
#
vboxmanage list vms
```

### beaglebone

```bash
#!/bin/bash
#

# var
VERSION='1.0'
AUTHOR='Frederico Sales'
DATA='11-03-2018'
EMAIL='<frederico.sales@engenharia.ufjf.br>'
SNAM=$(basename "$0")

# version
__VERSION__="
$SNAM version: $VERSION - $DATA

---------------
$AUTHOR
$EMAIL

"

# usage
__USAGE__="
           __
      w  c(..)o   (
       \__(-)    __)
           /\   ( 
          /(_)___) 
          w /| 
           | \ 
          m  m

[Usage]: $SNAM [OPTION]
    
    -h  - show this help;
    -i  - start vm $SNAM on headless mode;
    -s  - stop vm $SNAM;
    -v  - show version name.

"

start_service() {
    echo "Starting VM $SNAM.";
    vboxmanage startvm $SNAM --type headless;
    echo;
    echo "Done."
}

stop_service() {
    echo "Stopping VM $SNAM.";
    vboxmanage controlvm $SNAM poweroff; 
    echo;
    echo "Done."
}

# check argument
if [[ $# != 1 ]]; then
    printf "$__USAGE__"
    exit 1
fi

# argument
ARGUMENT=$(echo "$1" | awk '{print tolower($0)}')

# case
case $ARGUMENT in
    '-h')
        printf "$__USAGE__"
        ;;

    '-i')
        start_service
        ;;

    '-s')
        stop_service
        ;;

     '-v')
         printf "$__VERSION__"
         ;;

     *)
         printf "$__USAGE__"
         ;;
 esac
```
